// Water fragment shader for proj2 in cs594
// based on bump mapping shader from oZone3D.Net Tutorials and
// Realistic bump mapped water shader from Gametutorials.com

// light and view direction in tangent space
varying vec3 lightVec;
varying vec3 eyeVec;

// textures having reflected or refracted scene
uniform sampler2D reflectionMap;
uniform sampler2D refractionMap;

// DuDv Map gives variance in scene rendered on textures
// by giving distortion in textur coordinates
uniform sampler2D dudvMap;

// Normal map for light reflection
uniform sampler2D normalMap;

// winSize is used to calculate texture coordinate for
// scenes pre-rendered on textures from glFragCoord.xy
// glFragCoord is window coordinate of the fragment
// uniform vec2  winSize;
uniform int winWidth;
uniform int winHeight;

// for animation
//uniform vec2  coordOffset;
uniform float coordOffX;
uniform float coordOffY;

void main (void)
{
	// AND HERE!!!
	vec2 winSize = vec2(winWidth, winHeight);
	vec2 coordOffset = vec2(coordOffX, coordOffY);
	
	// Flip fragment coordinate system to (0,0) at top left corner instead of bottom left corner
	vec2 newFragCoord = gl_FragCoord.xy/winSize;
	newFragCoord.y = 1.0 - newFragCoord.y;
	
	// reflection/refraction constant
	const float refConst = 0.5;
	// refraction ratio
   const float kRefraction = 0.019;

	// by changing offset in texture coordinate for DuDv map
	// and normal map animate water surface
	vec2 dudvCoord = gl_TexCoord[0].st + coordOffset;
	
	// dudvColor decides distortion in reflection/refraction
   vec4 dudvColor = texture2D(dudvMap, dudvCoord);
   dudvColor = (dudvColor * 2.0 - 1.0) * kRefraction;

	// calculate texture coordinate of pre-rendered scene texture
	// current view is the same as the scene in the texture
	// we can easily calculate the appropriate coordinates
	// with dudv distortion
	//vec2 texCoord = gl_FragCoord.xy/winSize + dudvColor.xy;
	//vec4 reflectionColor = texture2D(reflectionMap, texCoord);
	//vec4 refractionColor = texture2D(refractionMap, texCoord);
	
	//Mine
	vec2 texCoord = newFragCoord+ dudvColor.xy;
	vec4 reflectionColor = texture2D(reflectionMap, texCoord);
	//vec4 reflectionColor = texture2D(reflectionMap, vTexBlah);
	vec4 refractionColor = texture2D(refractionMap, texCoord);
	
	
	
	// normal map for light reflection
	vec3 bump = normalize( texture2D(normalMap, dudvCoord).xyz * 2.0 - 1.0 );
	vec3 vVec = normalize(eyeVec);
	vec3 lightReflection = normalize(reflect(lightVec, bump));

	// add fresnel term for realistic reflection
   vec4 invertedFresnel = vec4( dot(bump, lightReflection) );
   vec4 fresnelTerm = 1.0 - invertedFresnel;

	
	reflectionColor *= (fresnelTerm);
   float intensity = max(0.0, dot(lightReflection, -vVec) );
	
	// remove specular reflection when the camera is in the water
	if (vVec.z > 0)
		intensity = 0;

   vec4 specular = vec4(pow(intensity, 128));
	
	// refraction + reflection = total light coming into water
	gl_FragColor = refConst*refractionColor + (1-refConst)*reflectionColor + specular;
}
