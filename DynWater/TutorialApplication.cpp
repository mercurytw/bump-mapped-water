/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _ 
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/                              
      Tutorial Framework
      http://www.ogre3d.org/tikiwiki/
-----------------------------------------------------------------------------
*/
#include "TutorialApplication.h"

// Pardon the C-style...
float lpos[3] = {0.0f, 30.0f, -3.0f};
unsigned int texSize[2] = {266, 200};
unsigned int winWidth, winHeight;
int whichTexPass = -1;
const float waterHeight = 45.0f;

float waterOffset[2] = {0.0, 0.0};
const float waterX = 0.001f, waterY = 0.001f;


//-------------------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//-------------------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}
//-------------------------------------------------------------------------------------
void TutorialApplication::generateCubeMesh(void)
{
	const float width = 50;
	const float height = 50;
	Ogre::ManualObject mo("CubeObject");

	Ogre::Vector3 vec(width/2, 0, width/2);
	Ogre::Quaternion rot;
	rot.FromAngleAxis(Ogre::Degree(90), Ogre::Vector3::UNIT_Y);

	mo.begin("Examples/BeachStones", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	for (int i = 0; i < 4; ++i)
	{
		if(i%2 == 0)
		{
			mo.position(-vec.x, height, -vec.z);
			mo.textureCoord(0, 0);

			mo.position(-vec.x, height, vec.z);
			mo.textureCoord(1, 0);

			mo.position(-vec.x, 0, -vec.z);
			mo.textureCoord(0, 1);

			mo.position(-vec.x, 0, vec.z);
			mo.textureCoord(1, 1);
		}
		else
		{
			mo.position(-vec.x, height, -vec.z);
			mo.textureCoord(0, 0);

			mo.position(vec.x, height, -vec.z);
			mo.textureCoord(1, 0);

			mo.position(-vec.x, 0, -vec.z);
			mo.textureCoord(0, 1);

			mo.position(vec.x, 0, -vec.z);
			mo.textureCoord(1, 1);
		}
		
		int offset = i * 4;
		mo.triangle(offset, offset+3, offset+1);
		mo.triangle(offset, offset+2, offset+3);
		mo.triangle(offset+1, offset+3, offset);
		mo.triangle(offset+3, offset+2, offset);


		
		vec = rot * vec;
	}
	mo.end();

	vec = Ogre::Vector3((width/2), height, (width/2));
	mo.begin("Examples/BeachStones", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	{
		/*mo.position(-vec.x, height, -vec.z);
		mo.textureCoord(0,0);

		mo.position(vec.x, height, -vec.z);
		mo.textureCoord(1,0);

		mo.position(-vec.x, height, vec.z);
		mo.textureCoord(0,1);

		mo.position(vec.x, height, vec.z);
		mo.textureCoord(1,1);*/

		//bottom
		//4
		mo.position(-vec.x, 0, -vec.z);
		mo.textureCoord(0,0);

		//5
		mo.position(vec.x, 0, -vec.z);
		mo.textureCoord(1,0);

		//6
		mo.position(-vec.x, 0, vec.z);
		mo.textureCoord(0,1);

		//7
		mo.position(vec.x, 0, vec.z);
		mo.textureCoord(1,1);

		mo.triangle(0, 3, 1);
		mo.triangle(0, 2, 3);
		//mo.triangle(4, 5, 6);
		//mo.triangle(7, 6, 5);
	}
	mo.end();

	/*mo.begin("Ocean2_HLSL_GLSL", Ogre::RenderOperation::OT_TRIANGLE_LIST);
	{
		mo.position(-vec.x, height, -vec.z);
		mo.textureCoord(0,0);

		mo.position(vec.x, height, -vec.z);
		mo.textureCoord(1,0);

		mo.position(-vec.x, height, vec.z);
		mo.textureCoord(0,1);

		mo.position(vec.x, height, vec.z);
		mo.textureCoord(1,1);

		mo.triangle(0, 3, 1);
		mo.triangle(0, 2, 3);
	}
	mo.end();*/

	mo.convertToMesh("CubeMesh");
}
//-------------------------------------------------------------------------------------
void TutorialApplication::createFrameListener(void)
{
	BaseApplication::createFrameListener();
}
//-------------------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    mCamera->setPosition(Ogre::Vector3(0.0f, 20.0f, -200.0f));
	mCamera->lookAt(Ogre::Vector3::ZERO);

	mSceneMgr->setAmbientLight(Ogre::ColourValue(0.25f, 0.25f, 0.25f));

	Ogre::Light* pointLight = mSceneMgr->createLight("PointLight");
	pointLight->setType(Ogre::Light::LT_POINT);
	pointLight->setDiffuseColour(Ogre::ColourValue::White);
	pointLight->setPosition(lpos[0], lpos[1], lpos[2]);


	Ogre::Plane plane(Ogre::Vector3::UNIT_Y, -1);
	Ogre::MeshManager::getSingleton().createPlane("ground", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        plane, 1500, 1500, 20, 20, true, 1, 5, 5, Ogre::Vector3::UNIT_Z);

	Ogre::Entity* entGround = mSceneMgr->createEntity("GroundEntity", "ground");
    mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(entGround);
	entGround->setMaterialName("Examples/Rockwall");
    entGround->setCastShadows(false);

	Ogre::Entity* entHouse = mSceneMgr->createEntity("HouseEnt", "tudorhouse.mesh");
	Ogre::SceneNode* houseNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	houseNode->attachObject(entHouse);
	houseNode->setPosition(-130.0f, 100.0f, 250.0f);
	houseNode->scale(0.35f,0.35f,0.35f);

	generateCubeMesh();

	Ogre::Entity* entCube = mSceneMgr->createEntity("CubeEntity", "CubeMesh");
	mWellNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("WellSceneNode");
	mWellNode->attachObject(entCube);
	entCube->setCastShadows(false);

	mWaterPlane = Ogre::Plane(Ogre::Vector3::UNIT_Y, waterHeight);
	Ogre::MeshManager::getSingleton().createPlane("WaterPlane", 
		Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
        mWaterPlane, 
		50, 
		50, 
		20, 
		20, 
		true, 
		1, 
		1, 
		1, 
		Ogre::Vector3::UNIT_Z);

	mWaterEnt = mSceneMgr->createEntity("WaterEntity", "WaterPlane");
    mWellNode->attachObject(mWaterEnt);
	mWaterEnt->setMaterialName("BaseWhiteNoLighting");
    mWaterEnt->setCastShadows(false);

	// Set up the textures!!
	// Reflection
	mReflectionTexPtr = Ogre::TextureManager::getSingleton().createManual("ReflectionRttTex", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
		Ogre::TEX_TYPE_2D, texSize[0], texSize[1], 0, Ogre::PF_R8G8B8, Ogre::TU_RENDERTARGET);

	mReflectionRTX = mReflectionTexPtr->getBuffer()->getRenderTarget();
	
	mReflectionRTX->addViewport(mCamera);
	mReflectionRTX->getViewport(0)->setClearEveryFrame(true);
	mReflectionRTX->getViewport(0)->setBackgroundColour(Ogre::ColourValue::Black);
	mReflectionRTX->getViewport(0)->setOverlaysEnabled(false);

	mReflectionRTX->addListener(this);
	mReflectionRTX->setAutoUpdated(false);

	//Ogre::MaterialPtr matPtr = Ogre::MaterialManager::getSingleton().getByName("myScripts/CheapWater");
	//matPtr->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setTextureName("ReflectionRttTex", Ogre::TextureType::TEX_TYPE_2D);

	// =================== Refraction ===================
	mRefractTexPtr = Ogre::TextureManager::getSingleton().createManual("RefractionRttTex", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, 
		Ogre::TEX_TYPE_2D, texSize[0], texSize[1], 0, Ogre::PF_R8G8B8, Ogre::TU_RENDERTARGET);

	mRefractRTX = mRefractTexPtr->getBuffer()->getRenderTarget();
	mRefractRTX->addViewport(mCamera);
	mRefractRTX->getViewport(0)->setClearEveryFrame(true);
	mRefractRTX->getViewport(0)->setBackgroundColour(Ogre::ColourValue::Black);
	mRefractRTX->getViewport(0)->setOverlaysEnabled(false);

	mRefractRTX->addListener(this);
	mRefractRTX->setAutoUpdated(false);

	mOldCPos = Ogre::Vector3(200.0f, 200.0f, 200.0f); // Garbage initial value to force update of RttTexes on first frame
	mOldCOrient = mCamera->getOrientation();

	
	winWidth = mWindow->getWidth();
	winHeight = mWindow->getHeight();
	int w,h;
	w = (int) winWidth;
	h = (int) winHeight;

	Ogre::LogManager::getSingleton().logMessage("<My Logging>: Window Width: " + Ogre::StringConverter::toString(w));
	Ogre::LogManager::getSingleton().logMessage("<My Logging>: Window Height: " + Ogre::StringConverter::toString(h));

	Ogre::MaterialPtr matPtr = Ogre::MaterialManager::getSingleton().getByName("myScripts/CheapWater");
	Ogre::GpuProgramParametersSharedPtr pParams = matPtr->getTechnique(0)->getPass(0)->getFragmentProgramParameters();

	

	pParams->setNamedConstant("winWidth", w);
	pParams->setNamedConstant("winHeight", h);

	mWaterEnt->setMaterialName("myScripts/CheapWater");
	
	mCamera->getViewport()->setBackgroundColour(Ogre::ColourValue(0.0f,0.0f,0.0f,0.0f));

	mSceneMgr->setSkyBox(true, "Examples/CloudyNoonSkyBox");
	mClipper = Ogre::Plane(mWaterPlane.normal, Ogre::Vector3(0.0f, 50.0f, 0.0f));
}
//-------------------------------------------------------------------------------------
bool TutorialApplication::frameRenderingQueued(const Ogre::FrameEvent &evt)
{	
	updateWaterOffsets(evt); // looking for this function? It's down at the very bottom of this file!
	
	if((mOldCPos != mCamera->getPosition()) || (mOldCOrient != mCamera->getOrientation()))
	{
		//mSceneMgr->getEntity("CubeEntity")->setVisible(true);
		mReflectionRTX->update(true);
		mRefractRTX->update(true);
		mOldCPos = mCamera->getPosition();
		mOldCOrient = mCamera->getOrientation();
		//mSceneMgr->getEntity("CubeEntity")->setVisible(false);
	}
	return BaseApplication::frameRenderingQueued(evt);
}
//-------------------------------------------------------------------------------------
void TutorialApplication::preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
	if((whichTexPass = (whichTexPass+1)%2) == 0) // It's Reflection time!
	{
		
		/*mSceneMgr->getRootSceneNode()->translate(0.0f, waterHeight, 0.0f, Ogre::Node::TS_WORLD);
		mSceneMgr->getRootSceneNode()->scale(1.0f, -1.0f, 1.0f);*/

		/*Ogre::Plane bob = mWaterPlane;
		bob.normal = bob.normal * Ogre::Vector3(-1.0, -1.0, -1.0);*/
		
		mCamera->enableCustomNearClipPlane(mClipper);//mWaterPlane);

		//// SCIENCE start ===============================
		//
		//// backup old cam infos
		mRefCamPos = mCamera->getPosition();
		/*Ogre::Vector3 oldDir = mCamera->getDirection();
		const Ogre::Vector3 planeCent = Ogre::Vector3(0.0f, waterHeight, 0.0f);

		Ogre::Vector3 inVect = planeCent - mRefCamPos;
		Ogre::Real disp = planeCent.distance(mRefCamPos);
			if(disp < 0.0f) {disp = -1.0 *disp; }*/
			/*Ogre::Ray bob(mRefCamPos, inVect);

			Ogre::Real dist = Ogre::Math::intersects(bob, mWaterPlane).second;*/
		
		

		//mCamera->moveRelative
		if(mRefCamPos.y >= waterHeight)
		{
			//mCamera->setPosition(0.0f, waterHeight, 0.0f);
			
			mCamera->enableReflection(mWaterPlane);

			/*Ogre::Vector3 aTry(0.0f, 0.0f, disp);
			mCamera->moveRelative(aTry);*/
#ifdef _DEBUG
			/*Ogre::Vector3 aTry(0.0f, 0.0f, 0.0f);
			Ogre::Vector3 newPos = mCamera->getPosition();
			Ogre::Vector3 newDir = mCamera->getDirection();
			if(newDir !=oldDir)
			{
				aTry = Ogre::Vector3::ZERO;
			}*/
#endif

			/*mCamera->moveRelative(Ogre::Vector3(0.0f, 0.0f, -10.0f));
			Ogre::Vector3 point =  mCamera->getPosition();
			mCamera->setPosition(0.0f, waterHeight, 0.0f);
			bob = Ogre::Ray(mCamera->getPosition(), point - mCamera->getPosition());
			Ogre::Vector3 finalLoc = bob.getPoint(dist);
			mCamera->setPosition(finalLoc);*/

			//aTry = Ogre::Vector3::ZERO;
		}
		
		mWaterEnt->setVisible(false);
		//mRefCamOrient = mCamera->getOrientation();
		//const Ogre::Vector3 planeCent = Ogre::Vector3(0.0f, waterHeight, 0.0f);
		//Ogre::Vector3 inVect = planeCent - mRefCamPos;

		////project look vector onto water plane
		//Ogre::Vector3 Vp = mWaterPlane.projectVector(inVect);

		//// find point of intersection
		//Ogre::Ray Rp = Ogre::Ray(mRefCamPos, inVect);
		//Ogre::Vector3 p_i = Rp.getPoint(Ogre::Math::intersects(Rp, mWaterPlane).second);

		//// find a third point
		//Ogre::Vector3 p_disp = p_i + Vp;

		//// build a plane & find it's normal
		//Ogre::Vector3 N = Ogre::Plane(mRefCamPos, p_i, p_disp).normal;

		//// invert direction of Vp
		//Ogre::Vector3 Vp_inverse = Vp * -1.0;

		//// get angle
		//Ogre::Radian theta = Vp_inverse.angleBetween(mRefCamPos - p_i);
		//if(theta < Ogre::Radian(0.0)){ theta = -1.0 * theta; }
		//theta = theta *2.0;

		////theta = Ogre::Degree(180.0f - theta.valueDegrees()).valueRadians();
		//Ogre::Quaternion quat(theta, N); // = Ogre::Quaternion::FromAngleAxis(0.0f, N);
		//
		//// set the camera =)
		//mCamera->setPosition(planeCent);
		//mCamera->rotate(quat);
		//// SCIENCE end =================================

		////mCamera->enableReflection(mWaterPlane);

		////mSceneMgr->getRootSceneNode()->translate(0.0f, -waterHeight, 0.0f, Ogre::Node::TS_WORLD);


		
	}
	else // It's refraction time!
	{
		//glDisable(GL_CLIP_PLANE0);
		
	}
}
//-------------------------------------------------------------------------------------
void TutorialApplication::postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt)
{
	if(whichTexPass == 0) // Reflection
	{
		mCamera->disableReflection();
		mCamera->disableCustomNearClipPlane();
		//mSceneMgr->getRootSceneNode()->resetToInitialState();
		mCamera->setPosition(mRefCamPos);
	}
	else // Refraction
	{
		//glEnable(GL_CLIP_PLANE0);
		mWaterEnt->setVisible(true);
	}
}
//-------------------------------------------------------------------------------------
void TutorialApplication::windowResized(Ogre::RenderWindow* rw)
{
	BaseApplication::windowResized(rw);
	
	winWidth = mWindow->getWidth();
	winHeight = mWindow->getHeight();
	int w,h;
	w = (int) winWidth;
	h = (int) winHeight;

	Ogre::LogManager::getSingleton().logMessage("<My Logging>: Window resize detected.");
	Ogre::LogManager::getSingleton().logMessage("<My Logging>: New Window Width: " + Ogre::StringConverter::toString(w));
	Ogre::LogManager::getSingleton().logMessage("<My Logging>: New Window Height: " + Ogre::StringConverter::toString(h));

	Ogre::MaterialPtr matPtr = Ogre::MaterialManager::getSingleton().getByName("myScripts/CheapWater");
	Ogre::GpuProgramParametersSharedPtr pParams = matPtr->getTechnique(0)->getPass(0)->getFragmentProgramParameters();
	
	pParams->setNamedConstant("winWidth", w);
	pParams->setNamedConstant("winHeight", h);

}
//-------------------------------------------------------------------------------------
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#include <float.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch( Ogre::Exception& e ) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox( NULL, e.getFullDescription().c_str(), "An exception has occured!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occured: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
//-----------------------------------------------------------
void TutorialApplication::updateWaterOffsets(const Ogre::FrameEvent& evt)
{
	Ogre::Real numOfClicks = (evt.timeSinceLastFrame * MILLISECONDS_PER_SECOND)/TIMER_FREQUENCY_MILLIS;
#ifdef WIN32_LEAN_AND_MEAN
	double check;
	waterOffset[0] = ((check = waterOffset[0] + (numOfClicks*waterX)) >= FLT_MAX) ? 0.0f : waterOffset[0] + (numOfClicks*waterX);
	waterOffset[1] = ((check = waterOffset[1] + (numOfClicks*waterY)) >= FLT_MAX) ? 0.0f : waterOffset[1] + (numOfClicks*waterY);
#else
	const float arbitrary_cap = 200.0f;
	waterOffset[0] = (waterOffset[0] + (numOfClicks*waterX) >= arbitrary_cap) ? 0.0f : waterOffset[0] + (numOfClicks*waterX);
	waterOffset[1] = (waterOffset[1] + (numOfClicks*waterY) >= arbitrary_cap) ? 0.0f : waterOffset[1] + (numOfClicks*waterY);
#endif 

	// Setup water offsets
	Ogre::MaterialPtr matPtr = Ogre::MaterialManager::getSingleton().getByName("myScripts/CheapWater");
	Ogre::GpuProgramParametersSharedPtr pParams = matPtr->getTechnique(0)->getPass(0)->getFragmentProgramParameters();
	
	pParams->setNamedConstant("coordOffX", waterOffset[0]);
	pParams->setNamedConstant("coordOffY", waterOffset[1]);
}
#endif
