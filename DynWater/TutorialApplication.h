/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.h
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _ 
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/                              
      Tutorial Framework
      http://www.ogre3d.org/tikiwiki/
-----------------------------------------------------------------------------
*/
/**
 * Referenced & Used:
 * Byungil Jeong's dynamic water from elearn, source at http://www.evl.uic.edu/bijeong/cs594/proj2/proj2.tar.gz
 * http://www.ogre3d.org/tikiwiki/Intermediate+Tutorial+7&structure=Tutorials
 * http://www.ogre3d.org/forums/viewtopic.php?f=2&t=60482&start=0
 **/


#ifndef __TutorialApplication_h_
#define __TutorialApplication_h_

#include "BaseApplication.h"



#define TIMER_FREQUENCY_MILLIS  20
#define MILLISECONDS_PER_SECOND 1000


class TutorialApplication : public BaseApplication, public Ogre::RenderTargetListener
{
public:
    TutorialApplication(void);
    virtual ~TutorialApplication(void);

protected:
	virtual void createFrameListener(void);
	virtual bool frameRenderingQueued(const Ogre::FrameEvent &evt);
	virtual void preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
	virtual void postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
	void generateCubeMesh(void);
    virtual void createScene(void);
	virtual void windowResized(Ogre::RenderWindow* rw);
	void updateWaterOffsets(const Ogre::FrameEvent& evt);

	Ogre::Plane mWaterPlane;
	Ogre::Entity* mWaterEnt;
	Ogre::SceneNode* mWellNode;
	Ogre::TexturePtr mReflectionTexPtr;
	Ogre::TexturePtr mRefractTexPtr;

	Ogre::RenderTexture *mReflectionRTX;
	Ogre::RenderTexture *mRefractRTX;

	Ogre::Vector3 mOldCPos;
	Ogre::Vector3 mRefCamPos;
	Ogre::Quaternion mRefCamOrient;
	Ogre::Quaternion mOldCOrient;
	Ogre::Plane backup;
	Ogre::Plane mClipper;
};

#endif // #ifndef __TutorialApplication_h_
