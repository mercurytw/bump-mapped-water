// Water vertex shader for proj2 in cs594
// based on bump mapping shader from oZone3D.Net Tutorials and
// Realistic bump mapped water shader from Gametutorials.com

uniform vec3 lightPosition;

// light and view direction in tangent space
varying vec3 lightVec; 
varying vec3 eyeVec;


void main(void)
{
	gl_Position = ftransform();
	gl_TexCoord[0] = gl_MultiTexCoord0;

	
	// automatic generation of tangential and binomal vec
	// from normal vector
	vec3 vTangent;
	if (gl_Normal.x == 0.0 && gl_Normal.z == 0.0)
		vTangent = vec3(1.0, 0.0, 0.0); 
	else
		vTangent = vec3(gl_Normal.z, 0, -gl_Normal.x); 		
	
	// transform to eye coordinates
	vec3 n = normalize(gl_NormalMatrix * gl_Normal);
	vec3 t = normalize(gl_NormalMatrix * vTangent);
	vec3 b = cross(n, t);
	
	// vertex in eye coordinate
	vec4 vVertex = gl_ModelViewMatrix * gl_Vertex;
	
	// light direction in eye coordinate
	vec3 tmpVec = vec3(gl_ModelViewMatrix * vec4(lightPosition, 1.0) - vVertex);
	
	// transform light vector into tangent space
	lightVec.x = dot(tmpVec, t);
	lightVec.y = dot(tmpVec, b);
	lightVec.z = dot(tmpVec, n);
	
	lightVec = normalize(lightVec);

	// transform view vector into tangent space
	tmpVec = -vVertex.xyz;
	eyeVec.x = dot(tmpVec, t);
	eyeVec.y = dot(tmpVec, b);
	eyeVec.z = dot(tmpVec, n);
	
	// for user clipping
	gl_ClipVertex = vVertex;
}
